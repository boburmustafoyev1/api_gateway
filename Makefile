fix~migrate1:
	migrate -path migrations/ -database postgres://postgres:3301@localhost:5432/postdb?sslmode=disable force 1

fix~migrate2:
	migrate -path migrations/ -database postgres://postgres:3301@localhost:5432/postdb?sslmode=disable force 2

migrate~up:
	migrate -source file://migrations/ -database postgres://postgres:3301@localhost:5432/postdb up

migrate~down:
	migrate -source file://migrations/ -database postgres://postgres:3301@localhost:5432/postdb down

pull_submodule:
	git submodule update --init --recursive

update_submodule:
	git submodule update --remote --merge

script:
	./script/gen-proto.sh

swag:
	swag init -g ./api/router.go -o api/docs