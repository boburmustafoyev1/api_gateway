package v1

import (
	"api_gateway/genproto/review"
	l "api_gateway/pkg/logger"
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create review...
// @Summary Create Review
// @Description review service create
// @Tags Review
// @Accept json
// @Produce json
// @Param body body review.ReviewRequest true "Review"
// @Success 200 {object} review.ReviewResponse
// @Failure 400 "Errorresponse"
// @Router /v1/review [post]
func (h *handlerV1) CreateReview(c *gin.Context) {
	var (
		body        review.ReviewRequest
		jsrsMarshal protojson.MarshalOptions
	)
	jsrsMarshal.UseEnumNumbers = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.ReviewService().CreateReview(ctx, &body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create reveiw", l.Error(err))
	}
	c.JSON(http.StatusCreated, response)
}

// get review
// @Summary Get Review
// @Description this will display the review information
// @Tags Review
// @Accept json
// @Produce json
// @Param id path int true "id"
// @Success 200 {object} review.ReviewResponse
// @Failure 400 "Errorresponse"
// @Router /v1/review/{id} [get]
func (h *handlerV1) GetReviewById(c *gin.Context) {
	var jsrsMarshal protojson.MarshalOptions
	jsrsMarshal.UseEnumNumbers = true
	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to parsing int")
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	respose, err := h.serviceManager.ReviewService().GetReviewById(ctx, &review.ReviewId{
		Id: id,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error id of review")
	}
	c.JSON(http.StatusOK, respose)
}

// update review
// @Summary Update Review
// @Description this updating information of review
// @Tags Review
// @Accept json
// @Produce json
// @Param body body review.ReviewResponse true "Review"
// @Success 200 {object} review.ReviewResponse
// @Failure 400 "Errorresponse"
// @Failure 500 "Errorresponse"
// @Router /v1/review/update [post]
func (h *handlerV1) UpdateReview(c *gin.Context) {
	var (
		body        review.ReviewUp
		jsrsMarshal protojson.MarshalOptions
	)
	jsrsMarshal.UseEnumNumbers = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.ReviewService().UpdateReview(ctx, &body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error update review", l.Error(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// delete review
// @Summary Delete Review
// @Description this deleting information of review
// @Tags Review
// @Accept json
// @Produce json
// @Param id path int true "id"
// @Success 200 {object} review.ReviewResponse
// @Failure 400 "ErrorResponse"
// @Router /v1/review/delete/{id} [delete]
func (h *handlerV1) DeleteReview(c *gin.Context) {
	var jsrsMarshal protojson.MarshalOptions
	jsrsMarshal.UseEnumNumbers = true

	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to parsing int")
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ReviewService().DeleteReview(ctx, &review.ReviewId{
		Id: id,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error while deleting review")
	}
	c.JSON(http.StatusOK, response)

}
