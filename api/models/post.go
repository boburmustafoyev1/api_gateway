package models

type Post struct {
	Owner_id    int
	Name        string
	Description string
}
type PostResponse struct {
	Id        int    `json:"id"`
	AuthorID  int    `json:"author_id"`
	Title     string `json:"title"`
	Createdat string `json:"createdat"`
	Updatedat string `json:"updatedat"`
}

type Customer struct {
	FirstName   string
	LastName    string
	Bio         string
	Addresses   []Address
	Email       string
	PhoneNumber string
}

type Address struct {
	Country string
	Street  string
}
