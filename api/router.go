package api

import (
	_ "api_gateway/api/docs"
	v1 "api_gateway/api/handlers/v1"
	"api_gateway/config"
	"api_gateway/pkg/logger"
	"api_gateway/services"

	"github.com/gin-gonic/gin"
	swaggerfile "github.com/swaggo/files"

	ginSwagger "github.com/swaggo/gin-swagger"
)

type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
}

func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
	})

	api := router.Group("/v1")
	// customer service post, get...
	api.POST("/customer", handlerV1.Create)
	api.GET("/customer/post/:id", handlerV1.GetCustomerPost)
	api.POST("/customer/update", handlerV1.UpdateCustomer)
	api.DELETE("/customer/delete/:id", handlerV1.DeleteCustomer)

	//post service post, get...
	api.POST("/posts", handlerV1.CreatePost)
	// api.GET("/posts/:id", handlerV1.GetPostById)
	api.GET("/posts/review/:id", handlerV1.GetPosReview)
	api.POST("/posts/update/", handlerV1.UpdatePost)
	api.DELETE("/posts/delete/:id", handlerV1.DeletePost)

	//review service post, get...
	api.POST("/review", handlerV1.CreateReview)
	api.GET("/review/:id", handlerV1.GetReviewById)
	api.POST("/review/update", handlerV1.UpdateReview)
	api.DELETE("/review/delete/:id", handlerV1.DeleteReview)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("swagger/*any", ginSwagger.WrapHandler(swaggerfile.Handler, url))

	return router
}
